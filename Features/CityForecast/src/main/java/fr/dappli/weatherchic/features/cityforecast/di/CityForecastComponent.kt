package fr.dappli.weatherchic.features.cityforecast.di

import dagger.BindsInstance
import dagger.Component
import fr.dappli.weatherchic.features.cityforecast.di.modules.CityForecastActivityModule
import fr.dappli.weatherchic.features.cityforecast.ui.CityForecastActivity
import fr.dappli.weatherchic.libraries.core.di.BaseActivityComponent
import fr.dappli.weatherchic.libraries.core.di.CoreComponent
import fr.dappli.weatherchic.libraries.network.di.NetworkComponent
import fr.dappli.weatherchic.repositories.openweatherrepository.di.OpenWeatherApiServiceModule
import fr.dappli.weatherchic.repositories.openweatherrepository.di.OpenWeatherRepositoryModule

@Component(
    dependencies = [
        CoreComponent::class,
        NetworkComponent::class],
    modules = [
        CityForecastActivityModule::class,
        OpenWeatherRepositoryModule::class,
        OpenWeatherApiServiceModule::class
    ]
)
interface CityForecastComponent : BaseActivityComponent<CityForecastActivity> {

    @Component.Factory
    interface Factory {

        fun create(
            coreComponent: CoreComponent,
            networkComponent: NetworkComponent,
            @BindsInstance activity: CityForecastActivity
        ): CityForecastComponent
    }
}