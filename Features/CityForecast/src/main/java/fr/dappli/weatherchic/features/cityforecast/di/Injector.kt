package fr.dappli.weatherchic.features.cityforecast.di

import fr.dappli.weatherchic.features.cityforecast.ui.CityForecastActivity
import fr.dappli.weatherchic.libraries.core.coreComponent
import fr.dappli.weatherchic.libraries.network.networkComponent

fun inject(activity: CityForecastActivity) {

    DaggerCityForecastComponent.factory()
        .create(activity.coreComponent, activity.networkComponent, activity)
        .inject(activity)
}