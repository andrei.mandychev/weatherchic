package fr.dappli.weatherchic.features.cityforecast.di.modules

import dagger.Module
import dagger.Provides
import fr.dappli.weatherchic.features.cityforecast.ui.CityForecastActivity
import fr.dappli.weatherchic.libraries.core.vo.Mode
import fr.dappli.weatherchic.libraries.navigation.CityForecastNavigator

@Module
class CityForecastActivityModule {

    @Provides
    fun provideMode(activity: CityForecastActivity): Mode {
        val bundle = requireNotNull(activity.intent?.extras)
        return requireNotNull(bundle.getParcelable(CityForecastNavigator.MODE_ID))
    }
}