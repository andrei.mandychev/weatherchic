package fr.dappli.weatherchic.features.cityforecast.ui

import android.os.Bundle
import android.view.LayoutInflater
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.isVisible
import fr.dappli.weatherchic.features.cityforecast.R
import fr.dappli.weatherchic.features.cityforecast.databinding.ActivityCityforecastBinding
import fr.dappli.weatherchic.features.cityforecast.di.inject
import fr.dappli.weatherchic.features.cityforecast.ui.adapter.WeatherItemAdapter
import fr.dappli.weatherchic.features.cityforecast.vm.CityForecastViewModel
import fr.dappli.weatherchic.features.cityforecast.vm.CityForecastViewModel.PageState
import fr.dappli.weatherchic.libraries.core.extensions.observeNotNull
import fr.dappli.weatherchic.libraries.core.vo.Mode
import fr.dappli.weatherchic.libraries.navigation.CityForecastNavigator
import javax.inject.Inject

class CityForecastActivity : AppCompatActivity() {

    @Inject
    lateinit var factory: CityForecastViewModel.Factory

    @Inject
    lateinit var navigator: CityForecastNavigator

    @Inject
    lateinit var mode: Mode

    private val viewModel by viewModels<CityForecastViewModel>(factoryProducer = { factory })

    private lateinit var binding: ActivityCityforecastBinding
    private lateinit var adapter: WeatherItemAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        inject(this)

        super.onCreate(savedInstanceState)
        binding = ActivityCityforecastBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)

        initToolbar()
        initViews()

        viewModel.state.observeNotNull(this, ::onPageStateReceived)
    }

    private fun initToolbar() {
        with (binding) {
            val toolbar = appBarLayout.findViewById<Toolbar>(fr.dappli.weatherchic.libraries.design.R.id.toolbar)
            setSupportActionBar(toolbar)
            supportActionBar?.apply {
                title = when (mode) {
                    is Mode.Short -> resources.getString(R.string.city_screen_title)
                    is Mode.Long -> resources.getString(R.string.details_screen_title)
                }
            }
        }
    }

    private fun initViews() {
        adapter = WeatherItemAdapter()
        binding.weatherRecyclerView.adapter = adapter
    }

    private fun onPageStateReceived(state: PageState) {
        when (state) {
            is PageState.Loading -> { /* TODO show loader */ }
            is PageState.Success -> {

                adapter.submitList(state.items)
                binding.seeMoreButton.apply {
                    isVisible = when (mode) {
                        is Mode.Short -> {
                            setOnClickListener {
                                startLongMode(state.cityId)
                            }
                            true
                        }
                        is Mode.Long -> false
                    }
                }
            }
            is PageState.Error -> { /* TODO show error */ }
        }
    }

    private fun startLongMode(cityId: String) {
        startActivity(navigator.newIntent(Mode.Long(cityId)))
    }
}