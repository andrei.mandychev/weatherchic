package fr.dappli.weatherchic.features.cityforecast.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import fr.dappli.weatherchic.features.cityforecast.R
import fr.dappli.weatherchic.features.cityforecast.vo.WeatherItem

class WeatherItemAdapter : ListAdapter<WeatherItem, WeatherItemViewHolder>(
    EditCardDiffCallback()
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherItemViewHolder {
        val context = parent.context
        val itemViewType = ViewType.values()[viewType]
        val view = LayoutInflater.from(context).inflate(itemViewType.layoutId, parent, false)
        return WeatherItemViewHolder(
            view
        )
    }

    override fun onBindViewHolder(holder: WeatherItemViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)
    }

    override fun getItemViewType(position: Int) =
        if (position == 0) {
            ViewType.CITY_ITEM
        } else {
            ViewType.NORMAL_ITEM
        }.ordinal

    enum class ViewType(@LayoutRes val layoutId: Int) {
        CITY_ITEM(R.layout.city_card),
        NORMAL_ITEM(R.layout.item_card)
    }

    class EditCardDiffCallback : DiffUtil.ItemCallback<WeatherItem>() {

        override fun areItemsTheSame(oldItem: WeatherItem, newItem: WeatherItem): Boolean {
            return oldItem.id == newItem.id
        }

        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(oldItem: WeatherItem, newItem: WeatherItem): Boolean {
            return oldItem == newItem
        }
    }
}