package fr.dappli.weatherchic.features.cityforecast.ui.adapter

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import fr.dappli.weatherchic.features.cityforecast.R
import fr.dappli.weatherchic.features.cityforecast.databinding.CityCardBinding
import fr.dappli.weatherchic.features.cityforecast.databinding.ItemCardBinding
import fr.dappli.weatherchic.features.cityforecast.vo.WeatherItem
import fr.dappli.weatherchic.features.cityforecast.vo.WeatherItem.Header
import fr.dappli.weatherchic.features.cityforecast.vo.WeatherItem.Item
import kotlinx.android.extensions.LayoutContainer

class WeatherItemViewHolder(
    override val containerView: View
) : RecyclerView.ViewHolder(containerView), LayoutContainer {

    private val context: Context = containerView.context

    fun bind(item: WeatherItem): Unit = when (item) {
        is Header -> bindHeader(item)
        is Item -> bindItem(item)
    }

    private fun bindHeader(header: Header) {
        with (CityCardBinding.bind(containerView)) {
            cityTextView.text = header.cityInformation.name
            dateTextView.text = header.cityInformation.date
            sunriseTextView.text = header.cityInformation.sunriseTime
            sunsetTextView.text = header.cityInformation.sunsetTime
        }
    }

    private fun bindItem(item: Item) {
        with (ItemCardBinding.bind(containerView)) {
            dateTextView.text = item.forecastItem.time
            temperatureTextView.text = context.resources.getString(
                R.string.temperature, item.forecastItem.mainForecast.temperature
            )
            feelsLikeTemperatureTextView.text = context.resources.getString(
                R.string.feels_like_temperature, item.forecastItem.mainForecast.feelsLikeTemperature
            )
        }

    }
}