package fr.dappli.weatherchic.features.cityforecast.vm

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fr.dappli.weatherchic.libraries.core.vo.Mode
import fr.dappli.weatherchic.features.cityforecast.vo.WeatherItem
import fr.dappli.weatherchic.libraries.core.vm.ViewModelFactory
import fr.dappli.weatherchic.usecases.cityforecastusecase.GetCityForecastUseCase
import fr.dappli.weatherchic.usecases.cityforecastusecase.GetMoreForecastUseCase
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

class CityForecastViewModel @Inject constructor(
    private val mode: Mode,
    val getCityForecastUseCase: GetCityForecastUseCase,
    val getMoreForecastUseCase: GetMoreForecastUseCase
): ViewModel() {

    private val _state = MutableLiveData<PageState>()

    val state: LiveData<PageState> get() = _state

    init {
        _state.value = PageState.Loading
        getCityForecast()
    }

    private fun getCityForecast() {
        viewModelScope.launch {
            try {
                val cityForecast = when (mode) {
                    is Mode.Short -> getCityForecastUseCase(mode.cityName)
                    is Mode.Long -> getMoreForecastUseCase(mode.cityId)
                }
                val items = cityForecast.let {
                    mutableListOf<WeatherItem>(WeatherItem.Header(it.cityInformation)).apply {
                        it.items.forEach { forecastItem ->
                            add(WeatherItem.Item(forecastItem))
                        }
                    }
                }
                _state.value = PageState.Success(cityForecast.cityInformation.id, items)
            } catch (ex: Exception) {
                _state.value = PageState.Error // TODO precise a type of error
            }
        }
    }

    class Factory @Inject constructor(
        vm: dagger.Lazy<CityForecastViewModel>
    ): ViewModelFactory<CityForecastViewModel>(vm)

    sealed class PageState {
        object Loading : PageState()
        class Success(val cityId: String, val items: List<WeatherItem>) : PageState()
        object Error : PageState()
    }
}