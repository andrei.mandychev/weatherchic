package fr.dappli.weatherchic.features.cityforecast.vo

import fr.dappli.weatherchic.usecases.cityforecastusecase.vo.CityInformation
import fr.dappli.weatherchic.usecases.cityforecastusecase.vo.ForecastItem

sealed class WeatherItem(val id: String) {

    data class Header(val cityInformation: CityInformation) : WeatherItem(cityInformation.id)
    data class Item(val forecastItem: ForecastItem) : WeatherItem(forecastItem.time)
}