package fr.dappli.weatherchic.features.citysearch.di

import dagger.BindsInstance
import dagger.Component
import fr.dappli.weatherchic.features.citysearch.ui.CitySearchActivity
import fr.dappli.weatherchic.libraries.core.di.BaseActivityComponent
import fr.dappli.weatherchic.libraries.core.di.CoreComponent
import fr.dappli.weatherchic.libraries.network.di.NetworkComponent
import fr.dappli.weatherchic.repositories.geogouvrepository.di.GeoGouvApiServiceModule
import fr.dappli.weatherchic.repositories.geogouvrepository.di.GeoGouvRepositoryModule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@Component(
    dependencies = [
        CoreComponent::class,
        NetworkComponent::class],
    modules = [
        GeoGouvRepositoryModule::class,
        GeoGouvApiServiceModule::class
    ]
)
@ExperimentalCoroutinesApi
@FlowPreview
interface CitySearchComponent : BaseActivityComponent<CitySearchActivity> {

    @Component.Factory
    interface Factory {

        fun create(
            coreComponent: CoreComponent,
            networkComponent: NetworkComponent,
            @BindsInstance activity: CitySearchActivity
        ): CitySearchComponent
    }
}