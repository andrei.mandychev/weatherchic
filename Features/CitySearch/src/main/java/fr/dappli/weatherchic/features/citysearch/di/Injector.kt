package fr.dappli.weatherchic.features.citysearch.di

import fr.dappli.weatherchic.features.citysearch.ui.CitySearchActivity
import fr.dappli.weatherchic.libraries.core.coreComponent
import fr.dappli.weatherchic.libraries.network.networkComponent

fun inject(activity: CitySearchActivity) {

    DaggerCitySearchComponent.factory()
        .create(activity.coreComponent, activity.networkComponent, activity)
        .inject(activity)
}