package fr.dappli.weatherchic.features.citysearch.ui

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.inputmethod.InputMethodManager
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.cursoradapter.widget.CursorAdapter
import com.google.android.material.snackbar.Snackbar
import fr.dappli.weatherchic.features.citysearch.R
import fr.dappli.weatherchic.features.citysearch.databinding.ActivityCitysearchBinding
import fr.dappli.weatherchic.features.citysearch.di.inject
import fr.dappli.weatherchic.features.citysearch.vm.CitySearchViewModel
import fr.dappli.weatherchic.features.citysearch.vm.CitySearchViewModel.Event
import fr.dappli.weatherchic.features.citysearch.vm.CitySearchViewModel.PageState
import fr.dappli.weatherchic.libraries.core.extensions.observeNotNull
import fr.dappli.weatherchic.libraries.core.vo.Mode
import fr.dappli.weatherchic.libraries.navigation.CityForecastNavigator
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Inject


@FlowPreview
@ExperimentalCoroutinesApi
class CitySearchActivity : AppCompatActivity() {

    @Inject
    lateinit var factory: CitySearchViewModel.Factory

    @Inject
    lateinit var navigator: CityForecastNavigator

    private lateinit var binding: ActivityCitysearchBinding

    private val viewModel by viewModels<CitySearchViewModel>(factoryProducer = { factory })

    private val cityAdapter: CursorAdapter by lazy { getCityCursorAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        inject(this)

        super.onCreate(savedInstanceState)
        binding = ActivityCitysearchBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)

        initToolbar()
        initViews()
        viewModel.state.observeNotNull(this, ::onStateReceived)
        viewModel.cities.observeNotNull(this, ::onCitiesReceived)
        viewModel.events.observeNotNull(this, ::onEventReceived)
    }

    private fun initToolbar() {
        with (binding) {
            val toolbar = appBarLayout.findViewById<Toolbar>(fr.dappli.weatherchic.libraries.design.R.id.toolbar)
            setSupportActionBar(toolbar)
            supportActionBar?.apply {
                title = resources.getString(R.string.city_search_title)
            }
        }
    }
    private fun initViews() {
        initSearchView()
        initNextButton()
    }

    private fun initSearchView() {
        with(binding.searchView) {
            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?) = true

                override fun onQueryTextChange(newText: String?): Boolean {
                    newText?.let { viewModel.onCityInputTextChanged(it) }
                    return true
                }
            })
            suggestionsAdapter = cityAdapter
            setSuggestionListener { viewModel.onCitySelected(it) }
            setCloseListener { viewModel.onCityCloseClicked() }
        }
    }

    private fun initNextButton() {
        binding.nextButton.setOnClickListener {
            viewModel.onNextButtonClicked()
        }
    }

    override fun onResume() {
        super.onResume()
        binding.searchView.clearFocus() // don't show a keyboard by default if we return
    }

    private fun onStateReceived(state: PageState) {
        binding.nextButton.isEnabled = when (state) {
            PageState.Idle -> false
            PageState.CitySelected -> true
        }
    }

    private fun onCitiesReceived(cities: List<String>) {
        cityAdapter.setCities(cities)
    }

    private fun onEventReceived(event: Event) {
        when (event) {
            is Event.CitySelected -> {
                startActivity(navigator.newIntent(Mode.Short(event.cityName)))
            }
            is Event.NextButtonClicked -> {
                startActivity(navigator.newIntent(Mode.Short(event.cityName)))
            }
            is Event.Error -> {
                // hide keyboard
                (getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager).apply {
                    hideSoftInputFromWindow(binding.searchView.windowToken, 0)
                }
                // show error
                val error = event.errorName ?: resources.getString(R.string.unknown_error)
                Snackbar.make(binding.content, error, Snackbar.LENGTH_SHORT)
                    .show()
            }
        }
    }

}