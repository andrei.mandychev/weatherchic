package fr.dappli.weatherchic.features.citysearch.ui

import android.app.SearchManager
import android.content.Context
import android.database.MatrixCursor
import android.provider.BaseColumns
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.cursoradapter.widget.CursorAdapter
import androidx.cursoradapter.widget.SimpleCursorAdapter

class SearchViewSuggestionListener(
    private val searchView: SearchView,
    private val callback: (Int) -> Unit
) : SearchView.OnSuggestionListener {

    override fun onSuggestionSelect(position: Int) = true

    override fun onSuggestionClick(position: Int): Boolean {
        val cursor = searchView.suggestionsAdapter.cursor
        cursor.moveToPosition(position)
        val suggestion: String = cursor.getString(textColumnIndex)
        searchView.setQuery(suggestion, true) // setting suggestion

        callback(position)

        return true
    }
}

fun SearchView.setSuggestionListener(callback: (Int) -> Unit) {
    this.setOnSuggestionListener(SearchViewSuggestionListener(this, callback))
}

fun SearchView.setCloseListener(callback: () -> Unit) {
    val view = findViewById<View>(androidx.appcompat.R.id.search_close_btn)
    view.setOnClickListener {
        this.setQuery("", true)
        callback()
    }
}

fun CursorAdapter.setCities(cities: List<String>) {
    val cursor = MatrixCursor(columns)

    cities.forEachIndexed { index, city ->
        cursor.addRow(
            arrayOf(
                index.toString(),
                city
            )
        )
    }

    changeCursor(cursor)
}

fun Context.getCityCursorAdapter(): CursorAdapter = SimpleCursorAdapter(
    this,
    android.R.layout.simple_list_item_1,
    null,
    arrayOf(SearchManager.SUGGEST_COLUMN_TEXT_1),
    intArrayOf(android.R.id.text1),
    0
)

private val columns = arrayOf(
    BaseColumns._ID,
    SearchManager.SUGGEST_COLUMN_TEXT_1
)

private val textColumnIndex = columns.indexOf(SearchManager.SUGGEST_COLUMN_TEXT_1)