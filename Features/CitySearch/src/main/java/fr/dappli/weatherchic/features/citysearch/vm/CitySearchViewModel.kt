package fr.dappli.weatherchic.features.citysearch.vm

import androidx.lifecycle.*
import fr.dappli.weatherchic.libraries.core.livedata.SingleLiveEvent
import fr.dappli.weatherchic.libraries.core.vm.ViewModelFactory
import fr.dappli.weatherchic.usecases.citysearchusecase.GetCitiesUseCase
import fr.dappli.weatherchic.usecases.citysearchusecase.vo.City
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.*
import java.lang.Exception
import javax.inject.Inject

@ExperimentalCoroutinesApi
@FlowPreview
class CitySearchViewModel @Inject constructor(
    private val getCitiesUseCase: GetCitiesUseCase
): ViewModel() {

    private var selectedCity: City? = null
    private var citiesList = emptyList<City>()
    private val userInputFlow = MutableStateFlow("")
    private val _events = SingleLiveEvent<Event>()
    private val _state = MutableLiveData<PageState>()

    val cities: LiveData<List<String>> = userInputFlow
        .debounce(DEBOUNCE_TIME_MS)
        .filter { query ->
            return@filter query.isNotEmpty()
        }
        .distinctUntilChanged()
        .mapLatest {
            try {
                getCitiesUseCase(it).also { data ->
                    citiesList = data
                }
                .map { city -> city.zipName }
            } catch (ex: Exception) { // we should catch exceptions here, otherwise the coroutine will be stopped.
                _state.value = PageState.Idle
                _events.value = Event.Error(ex.message)
                emptyList<String>()
            }
        }
        .asLiveData()

    val events: LiveData<Event> get() = _events

    val state: LiveData<PageState> get() = _state

    init {
        _state.value = PageState.Idle
    }

    fun onCityInputTextChanged(cityName: String) {
        userInputFlow.value = cityName
    }

    fun onCitySelected(cityPosition: Int) {
        val city = citiesList[cityPosition]
        selectedCity = city
        _state.value = PageState.CitySelected
        _events.value = Event.CitySelected(city.cityName)
    }

    fun onNextButtonClicked() {
        selectedCity?.let {
            _events.value = Event.NextButtonClicked(it.cityName)
        } ?: run {
            // TODO report issue, next button must be disabled if selected city is null
        }
    }

    fun onCityCloseClicked() {
        selectedCity = null
        _state.value = PageState.Idle
    }

    class Factory @Inject constructor(
        vm: dagger.Lazy<CitySearchViewModel>
    ): ViewModelFactory<CitySearchViewModel>(vm)

    sealed class PageState {
        object Idle : PageState()
        object CitySelected : PageState()
    }

    sealed class Event {
        class CitySelected(val cityName: String) : Event()
        class NextButtonClicked(val cityName: String) : Event()
        class Error(val errorName: String?) : Event()
    }

    private companion object {
        const val DEBOUNCE_TIME_MS = 300L
    }
}