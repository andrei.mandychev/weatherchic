package fr.dappli.weatherchic.libraries.core

import android.content.Context
import fr.dappli.weatherchic.libraries.core.di.CoreComponent

interface CoreComponentProvider {
    val coreComponent: CoreComponent
}

val Context.coreComponent: CoreComponent
    get() = (applicationContext as CoreComponentProvider).coreComponent
