package fr.dappli.weatherchic.libraries.core.di

import androidx.appcompat.app.AppCompatActivity

interface BaseComponent<T> {

    fun inject(target: T)
}

/**
 * Base dagger component for use in activities.
 */
interface BaseActivityComponent<T : AppCompatActivity> : BaseComponent<T>

