package fr.dappli.weatherchic.libraries.core.di

import android.app.Application
import android.content.Context
import com.google.gson.Gson
import dagger.BindsInstance
import dagger.Component
import fr.dappli.weatherchic.libraries.core.di.modules.CoreModule
import fr.dappli.weatherchic.libraries.core.di.modules.GsonModule

@Component(
    modules = [
        CoreModule::class,
        GsonModule::class
    ]
)
interface CoreComponent {

    fun application(): Application

    fun applicationContext(): Context

    fun gson(): Gson

    @Component.Factory interface Factory {
        fun create(@BindsInstance application: Application): CoreComponent
    }
}