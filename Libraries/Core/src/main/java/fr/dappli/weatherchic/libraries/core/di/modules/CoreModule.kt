package fr.dappli.weatherchic.libraries.core.di.modules

import android.app.Application
import android.content.Context
import dagger.Binds
import dagger.Module

@Module
internal abstract class CoreModule {

    @Binds
    abstract fun bindContext(application: Application): Context
}