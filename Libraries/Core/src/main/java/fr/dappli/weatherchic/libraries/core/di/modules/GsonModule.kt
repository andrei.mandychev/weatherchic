package fr.dappli.weatherchic.libraries.core.di.modules

import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.Reusable

@Module
internal class GsonModule {

    @Provides
    @Reusable
    fun provideGson() = GsonBuilder().create()
}