package fr.dappli.weatherchic.libraries.core.extensions

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer

// Setups observer that is triggered by non-null items only
fun <T> LiveData<T>.observeNotNull(owner: LifecycleOwner, observer: (t: T) -> Unit) {
    this.observe(owner, Observer { it?.let(observer) })
}

fun <T> LiveData<T>.observeNullable(owner: LifecycleOwner, observer: (t: T?) -> Unit) {
    this.observe(owner, Observer { observer(it) })
}

fun <T> MutableLiveData<List<T>>.updateList(onList: MutableList<T>.() -> Unit) {
    value = (value?.toMutableList() ?: mutableListOf()).apply { onList() }
}