package fr.dappli.weatherchic.libraries.core.vo

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

sealed class Mode : Parcelable {

    @Parcelize
    class Short(val cityName: String) : Mode()

    @Parcelize
    class Long(val cityId: String) : Mode()
}