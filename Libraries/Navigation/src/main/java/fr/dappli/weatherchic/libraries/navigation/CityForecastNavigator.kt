package fr.dappli.weatherchic.libraries.navigation

import android.content.Context
import android.content.Intent
import fr.dappli.weatherchic.libraries.core.vo.Mode
import javax.inject.Inject

class CityForecastNavigator @Inject constructor(private val context: Context) {

    fun newIntent(mode: Mode): Intent {
        return Intent().apply {
            setClassName(context.packageName, CITY_FORECAST_ACTIVITY_CLASS_NAME)
            putExtra(MODE_ID, mode)
        }
    }


    companion object {
        const val CITY_FORECAST_ACTIVITY_CLASS_NAME =
            "fr.dappli.weatherchic.features.cityforecast.ui.CityForecastActivity"
        const val MODE_ID = "MODE_ID"
    }
}