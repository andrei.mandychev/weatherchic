package fr.dappli.weatherchic.libraries.navigation

import android.content.Context
import android.content.Intent
import javax.inject.Inject

class CitySearchNavigator @Inject constructor(private val context: Context) {

    fun newIntent(): Intent {
        return Intent().apply {
            setClassName(context.packageName, CITY_SEARCH_ACTIVITY_CLASS_NAME)
        }
    }


    companion object {
        const val CITY_SEARCH_ACTIVITY_CLASS_NAME =
            "fr.dappli.weatherchic.features.citysearch.ui.CitySearchActivity"
    }
}