package fr.dappli.weatherchic.libraries.network

import android.content.Context
import fr.dappli.weatherchic.libraries.network.di.NetworkComponent

interface NetworkComponentProvider {
    val networkComponent: NetworkComponent
}

val Context.networkComponent: NetworkComponent
    get() = (applicationContext as NetworkComponentProvider).networkComponent
