package fr.dappli.weatherchic.libraries.network.di

import dagger.Component
import fr.dappli.weatherchic.libraries.core.di.CoreComponent
import fr.dappli.weatherchic.libraries.network.di.modules.NetworkModule
import retrofit2.Retrofit
import javax.inject.Named

@Component(
    dependencies = [CoreComponent::class],
    modules = [NetworkModule::class]
)
interface NetworkComponent {

    @Named(NetworkModule.OPEN_WEATHER_RETROFIT)
    fun openWeatherRetrofit(): Retrofit

    @Named(NetworkModule.GEO_GOUV_RETROFIT)
    fun geoGouvRetrofit(): Retrofit

    @Component.Factory interface Factory {
        fun create(coreComponent: CoreComponent): NetworkComponent
    }
}