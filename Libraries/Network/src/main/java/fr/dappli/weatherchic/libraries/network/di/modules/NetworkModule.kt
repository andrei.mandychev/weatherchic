package fr.dappli.weatherchic.libraries.network.di.modules

import android.content.Context
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.Reusable
import fr.dappli.weatherchic.libraries.network.interceptors.AuthorizationInterceptor
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import javax.inject.Named

@Module
class NetworkModule {

    @Provides
    @Reusable
    fun provideHttpCache(context: Context) =
        Cache(File(context.cacheDir, CACHE_DIRECTORY), CACHE_SIZE.toLong())

    @Provides
    @Reusable
    @Named(OPEN_WEATHER_HTTP_CLIENT)
    fun provideOpenWeatherOkHttpClient(
        cache: Cache,
        autorizationInterceptor: AuthorizationInterceptor
    ): OkHttpClient = OkHttpClient.Builder()
            .addNetworkInterceptor(autorizationInterceptor)
            .cache(cache)
            .build()

    @Provides
    @Reusable
    @Named(OPEN_WEATHER_RETROFIT)
    fun provideOpenWeatherRetrofit(
        gson: Gson,
        @Named(OPEN_WEATHER_HTTP_CLIENT) okHttpClient: OkHttpClient
    ): Retrofit = Retrofit.Builder()
        .baseUrl("http://api.openweathermap.org") // TODO move the url to config class
        .addConverterFactory(GsonConverterFactory.create(gson))
        .client(okHttpClient)
        .build()

    @Provides
    @Reusable
    @Named(GEO_GOUV_HTTP_CLIENT)
    fun provideGeoGouvOkHttpClient(
        cache: Cache
    ): OkHttpClient = OkHttpClient.Builder()
        .cache(cache)
        .build()

    @Provides
    @Reusable
    @Named(GEO_GOUV_RETROFIT)
    fun provideGeoGouvRetrofit(
        gson: Gson,
        @Named(GEO_GOUV_HTTP_CLIENT) okHttpClient: OkHttpClient
    ): Retrofit = Retrofit.Builder()
        .baseUrl("http://api-adresse.data.gouv.fr") // TODO move the url to config class
        .addConverterFactory(GsonConverterFactory.create(gson))
        .client(okHttpClient)
        .build()

    companion object {
        private const val CACHE_DIRECTORY = "HttpResponseCache"
        private const val CACHE_SIZE = 10 * 1024 * 1024

        const val OPEN_WEATHER_HTTP_CLIENT = "OpenWeatherHttpClient"
        const val OPEN_WEATHER_RETROFIT = "OpenWeatherRetrofit"
        const val GEO_GOUV_HTTP_CLIENT = "GeoGouvHttpClient"
        const val GEO_GOUV_RETROFIT = "GeoGouvRetrofit"
    }
}