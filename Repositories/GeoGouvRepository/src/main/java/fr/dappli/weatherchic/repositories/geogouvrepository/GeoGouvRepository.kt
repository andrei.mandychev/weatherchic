package fr.dappli.weatherchic.repositories.geogouvrepository

import fr.dappli.weatherchic.repositories.geogouvrepository.vo.GeoGouvSearchResponse

interface GeoGouvRepository {

    suspend fun getCities(cityName: String): GeoGouvSearchResponse
}