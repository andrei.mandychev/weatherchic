package fr.dappli.weatherchic.repositories.geogouvrepository

import fr.dappli.weatherchic.repositories.geogouvrepository.vo.GeoGouvSearchResponse
import javax.inject.Inject

class GeoGouvRepositoryImpl @Inject constructor(
    private val apiService: GeoGouvApiService
) : GeoGouvRepository {

    override suspend fun getCities(cityName: String): GeoGouvSearchResponse {
        // TODO handle exceptions
        return apiService.getCities(cityName)
    }
}