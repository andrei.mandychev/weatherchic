package fr.dappli.weatherchic.repositories.geogouvrepository.di

import dagger.Module
import dagger.Provides
import fr.dappli.weatherchic.libraries.network.di.modules.NetworkModule
import fr.dappli.weatherchic.repositories.geogouvrepository.GeoGouvApiService
import retrofit2.Retrofit
import javax.inject.Named

@Module
class GeoGouvApiServiceModule {

    @Provides
    fun provideGeoGouvApiService(
        @Named(NetworkModule.GEO_GOUV_RETROFIT) retrofit: Retrofit
    ): GeoGouvApiService = retrofit.create(GeoGouvApiService::class.java)
}