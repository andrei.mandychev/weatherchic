package fr.dappli.weatherchic.repositories.geogouvrepository.di

import dagger.Binds
import dagger.Module
import fr.dappli.weatherchic.repositories.geogouvrepository.GeoGouvRepository
import fr.dappli.weatherchic.repositories.geogouvrepository.GeoGouvRepositoryImpl

@Module
abstract class GeoGouvRepositoryModule {

    @Binds
    abstract fun bindGeoGouvRepository(repositoryImpl: GeoGouvRepositoryImpl): GeoGouvRepository
}