package fr.dappli.weatherchic.repositories.openweatherrepository

import fr.dappli.weatherchic.repositories.openweatherrepository.vo.WeatherForecastResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface OpenWeatherApiService {

    @GET("data/2.5/forecast")
    suspend fun getWeatherForecast(
        @Query(CITY) city: String,
        @Query(UNITS) units: String,
        @Query(COUNT) count: Int
    ): WeatherForecastResponse

    @GET("data/2.5/forecast")
    suspend fun getMoreWeatherForecast(
        @Query(ID) cityId: String,
        @Query(UNITS) units: String
    ): WeatherForecastResponse

    private companion object {
        const val CITY = "q"
        const val ID = "id"
        const val UNITS = "units"
        const val COUNT = "cnt"
    }
}