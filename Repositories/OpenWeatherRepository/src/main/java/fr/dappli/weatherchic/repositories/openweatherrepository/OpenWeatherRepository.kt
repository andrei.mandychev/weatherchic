package fr.dappli.weatherchic.repositories.openweatherrepository

import fr.dappli.weatherchic.repositories.openweatherrepository.vo.Definitions
import fr.dappli.weatherchic.repositories.openweatherrepository.vo.WeatherForecastResponse

interface OpenWeatherRepository {

    suspend fun getWeatherForecast(city: String, @Definitions.Units units: String, count: Int): WeatherForecastResponse

    suspend fun getMoreWeatherForecast(cityId: String, @Definitions.Units units: String): WeatherForecastResponse
}