package fr.dappli.weatherchic.repositories.openweatherrepository

import fr.dappli.weatherchic.repositories.openweatherrepository.vo.Definitions
import fr.dappli.weatherchic.repositories.openweatherrepository.vo.WeatherForecastResponse
import retrofit2.HttpException
import javax.inject.Inject

class OpenWeatherRepositoryImpl @Inject constructor(
    private val apiService: OpenWeatherApiService
) : OpenWeatherRepository {

    override suspend fun getWeatherForecast(city: String, @Definitions.Units units: String, count: Int): WeatherForecastResponse {
        try {
            return apiService.getWeatherForecast(city, units, count)
        } catch (ex: HttpException) {
            throw BackendException(ex.message())
        }
    }

    override suspend fun getMoreWeatherForecast(cityId: String, @Definitions.Units units: String): WeatherForecastResponse {
        try {
            return apiService.getMoreWeatherForecast(cityId, units)
        } catch (ex: HttpException) {
            throw BackendException(ex.message())
        }
    }

    // Create a kind of abstraction for use case
    class BackendException(message: String) : Exception(message)
}