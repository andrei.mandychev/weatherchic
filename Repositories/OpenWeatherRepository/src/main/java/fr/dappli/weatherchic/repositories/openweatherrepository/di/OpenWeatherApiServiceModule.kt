package fr.dappli.weatherchic.repositories.openweatherrepository.di

import dagger.Module
import dagger.Provides
import fr.dappli.weatherchic.libraries.network.di.modules.NetworkModule
import fr.dappli.weatherchic.repositories.openweatherrepository.OpenWeatherApiService
import retrofit2.Retrofit
import javax.inject.Named

@Module
class OpenWeatherApiServiceModule {

    @Provides
    fun provideOpenWeatherApiService(
        @Named(NetworkModule.OPEN_WEATHER_RETROFIT) retrofit: Retrofit
    ): OpenWeatherApiService = retrofit.create(OpenWeatherApiService::class.java)
}