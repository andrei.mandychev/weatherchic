package fr.dappli.weatherchic.repositories.openweatherrepository.di

import dagger.Binds
import dagger.Module
import fr.dappli.weatherchic.repositories.openweatherrepository.OpenWeatherRepository
import fr.dappli.weatherchic.repositories.openweatherrepository.OpenWeatherRepositoryImpl

@Module
abstract class OpenWeatherRepositoryModule {

    @Binds
    abstract fun bindOpenWeatherRepository(repositoryImpl: OpenWeatherRepositoryImpl): OpenWeatherRepository
}