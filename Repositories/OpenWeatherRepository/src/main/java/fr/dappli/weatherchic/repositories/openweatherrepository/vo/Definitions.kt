package fr.dappli.weatherchic.repositories.openweatherrepository.vo

import androidx.annotation.StringDef

class Definitions {

    @Retention(AnnotationRetention.SOURCE)
    @StringDef(METRIC) // TODO add other units
    annotation class Units

    companion object {
        // Units
        const val METRIC = "metric"

        // Counts
        const val DEFAULT_COUNT = 5
    }
}