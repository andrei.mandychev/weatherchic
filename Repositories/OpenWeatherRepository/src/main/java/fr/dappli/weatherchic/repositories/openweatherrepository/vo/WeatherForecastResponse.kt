package fr.dappli.weatherchic.repositories.openweatherrepository.vo

import com.google.gson.annotations.SerializedName

data class WeatherForecastResponse(
    // TODO add other fields
    @SerializedName("cnt") val count: Int?,
    @SerializedName("city") val city: City?,
    @SerializedName("list") val items: List<Item>?
) {
    data class City(
        @SerializedName("id") val id: Long?,
        @SerializedName("name") val name: String?,
        @SerializedName("country") val country: String?,
        @SerializedName("population") val population: Long?,
        @SerializedName("sunrise") val sunriseEpochTime: Long?,
        @SerializedName("sunset") val sunsetEpochTime: Long?
    )

    data class Item(
        @SerializedName("dt") val epochTime: Long?,
        @SerializedName("main") val mainInformation: MainInformation?
    ) {

        data class MainInformation(
            @SerializedName("temp") val temperature: Float?,
            @SerializedName("feels_like") val feelsLikeTemperature: Float?
        )
    }
}