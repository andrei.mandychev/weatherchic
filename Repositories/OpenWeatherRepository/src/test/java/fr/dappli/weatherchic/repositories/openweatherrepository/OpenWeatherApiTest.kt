package fr.dappli.weatherchic.repositories.openweatherrepository

import fr.dappli.weatherchic.repositories.openweatherrepository.di.inject
import fr.dappli.weatherchic.repositories.openweatherrepository.vo.Definitions
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import javax.inject.Inject

@RunWith(RobolectricTestRunner::class)
class OpenWeatherApiTest {

    @Inject
    lateinit var repo: OpenWeatherRepository

    init {
        inject(this)
    }

    @Test
    fun `test the first api call`() {
        val cityName = "Paris"
        val count = Definitions.DEFAULT_COUNT
        runBlocking {
            // We test a real api call, if it is not necessary, we can mock responses
            // by MockWebServer
            val response = repo.getWeatherForecast(cityName, Definitions.METRIC, count)
            Assert.assertEquals(response.count, count)
            Assert.assertEquals(response.city!!.name, cityName)
            Assert.assertEquals(response.city!!.country!!.toLowerCase(), "FR".toLowerCase())
            Assert.assertEquals(response.items!!.size, count)
        }
    }

    @Test
    fun `test the second api call`() {
        val cityId = "2988507"
        runBlocking {
            // We test a real api call, if it is not necessary, we can mock responses
            // by MockWebServer
            val response = repo.getMoreWeatherForecast(cityId, Definitions.METRIC)
            Assert.assertTrue(response.count!! > Definitions.DEFAULT_COUNT)
            Assert.assertEquals(response.city!!.name, "Paris")
            Assert.assertEquals(response.city!!.country!!.toLowerCase(), "FR".toLowerCase())
        }
    }
}