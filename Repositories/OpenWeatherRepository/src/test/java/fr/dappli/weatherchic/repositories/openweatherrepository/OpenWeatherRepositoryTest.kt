package fr.dappli.weatherchic.repositories.openweatherrepository

import fr.dappli.weatherchic.repositories.openweatherrepository.vo.Definitions
import fr.dappli.weatherchic.repositories.openweatherrepository.vo.WeatherForecastResponse
import fr.dappli.weatherchic.repositories.openweatherrepository.vo.WeatherForecastResponse.City
import io.mockk.coEvery
import io.mockk.mockk
import io.mockk.slot
import kotlinx.coroutines.test.runBlockingTest
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Assert
import org.junit.Test
import retrofit2.HttpException
import retrofit2.Response
import kotlin.random.Random

class OpenWeatherRepositoryTest {

    private val api = mockk<OpenWeatherApiService>()
    private val repository = OpenWeatherRepositoryImpl(api)

    @Test
    fun testNormalResult() {
        // Given
        val city = Random.nextInt().toString() // don't care
        val unitsParam = slot<String>()
        val countParam = slot<Int>()
        coEvery { api.getWeatherForecast(city, capture(unitsParam), capture(countParam)) } returns
                WeatherForecastResponse(
                    Definitions.DEFAULT_COUNT,
                    City(
                        id = Random.nextLong(),
                        name = city,
                        country = "FR",
                        population = Random.nextLong(),
                        sunriseEpochTime = Random.nextLong(),
                        sunsetEpochTime = Random.nextLong()
                    ),
                    items = emptyList()
                )

        runBlockingTest {
            // When
            val response = repository.getWeatherForecast(city, Definitions.METRIC, Definitions.DEFAULT_COUNT)

            // Then
            Assert.assertEquals(unitsParam.captured, Definitions.METRIC)
            Assert.assertEquals(countParam.captured, response.count)
            Assert.assertEquals(city, response.city!!.name)
        }
    }

    @Test
    fun testHttpException() {
        // Given
        val city = Random.nextInt().toString() // don't care
        val errorResponse = Response.error<WeatherForecastResponse>(
            401,
            "".toResponseBody("application/json".toMediaTypeOrNull())
        )
        coEvery { api.getWeatherForecast(any(), any(), any()) } throws HttpException(errorResponse)

        runBlockingTest {
            try {
                // When
                val response = repository.getWeatherForecast(city, Definitions.METRIC, Definitions.DEFAULT_COUNT)
                Assert.assertTrue(false) // must not happened

            } catch (ex: Exception) {

                // Then
                Assert.assertTrue(ex is OpenWeatherRepositoryImpl.BackendException)
            }
        }
    }
}