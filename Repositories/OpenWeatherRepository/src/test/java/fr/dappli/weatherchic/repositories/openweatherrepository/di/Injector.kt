package fr.dappli.weatherchic.repositories.openweatherrepository.di

import fr.dappli.weatherchic.repositories.openweatherrepository.OpenWeatherApiTest

fun inject(test: OpenWeatherApiTest) {
    DaggerTestNetworkComponent.factory()
        .create()
        .inject(test)
}