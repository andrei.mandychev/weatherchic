package fr.dappli.weatherchic.repositories.openweatherrepository.di

import dagger.Component
import fr.dappli.weatherchic.libraries.network.di.modules.NetworkModule
import fr.dappli.weatherchic.repositories.openweatherrepository.OpenWeatherApiTest
import fr.dappli.weatherchic.repositories.openweatherrepository.di.modules.TestCoreModule

@Component(
    modules = [
        NetworkModule::class,
        TestCoreModule::class,
        OpenWeatherApiServiceModule::class,
        OpenWeatherRepositoryModule::class
    ]
)
interface TestNetworkComponent { // TODO this component might be common for other repository tests

    fun inject(test: OpenWeatherApiTest)

    @Component.Factory interface Factory {
        fun create(): TestNetworkComponent
    }
}