package fr.dappli.weatherchic.repositories.openweatherrepository.di.modules

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.Reusable

@Module
class TestCoreModule {

    @Provides
    @Reusable
    fun provideGson() = GsonBuilder().create()

    @Provides
    @Reusable
    fun context(): Context = ApplicationProvider.getApplicationContext()
}