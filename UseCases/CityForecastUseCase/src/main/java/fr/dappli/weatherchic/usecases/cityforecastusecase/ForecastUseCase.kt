package fr.dappli.weatherchic.usecases.cityforecastusecase

import fr.dappli.weatherchic.repositories.openweatherrepository.vo.WeatherForecastResponse
import fr.dappli.weatherchic.usecases.cityforecastusecase.vo.CityForecast
import fr.dappli.weatherchic.usecases.cityforecastusecase.vo.CityInformation
import fr.dappli.weatherchic.usecases.cityforecastusecase.vo.ForecastItem
import fr.dappli.weatherchic.usecases.cityforecastusecase.vo.MainForecast
import fr.dappli.weatherchic.usecases.cityforecastusecase.ForecastUseCase.ProtocolException.NoCityException
import fr.dappli.weatherchic.usecases.cityforecastusecase.ForecastUseCase.ProtocolException.NoCityNameException
import fr.dappli.weatherchic.usecases.cityforecastusecase.ForecastUseCase.ProtocolException.NoCityIdException
import fr.dappli.weatherchic.usecases.cityforecastusecase.ForecastUseCase.ProtocolException.NoSunriseEpochTimeException
import fr.dappli.weatherchic.usecases.cityforecastusecase.ForecastUseCase.ProtocolException.NoSunsetEpochTimeException
import fr.dappli.weatherchic.usecases.cityforecastusecase.ForecastUseCase.ProtocolException.NoItemsException
import fr.dappli.weatherchic.usecases.cityforecastusecase.ForecastUseCase.ProtocolException.NoItemEpochTimeException
import fr.dappli.weatherchic.usecases.cityforecastusecase.ForecastUseCase.ProtocolException.NoMainTemperatureException
import fr.dappli.weatherchic.usecases.cityforecastusecase.ForecastUseCase.ProtocolException.NoMainFeelsLikeTemperatureException
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

open class ForecastUseCase {
    
    protected fun mapToCityForecast(response: WeatherForecastResponse) : CityForecast {
        val city = response.city ?: throw NoCityException
        val cityId = city.id?.toString() ?: throw NoCityIdException
        val cityName = city.name ?: throw NoCityNameException
        val sunrise = city.sunriseEpochTime ?: throw NoSunriseEpochTimeException
        val sunset = city.sunsetEpochTime ?: throw NoSunsetEpochTimeException

        val items = response.items ?: throw NoItemsException
        val forecastItems = items.map {
            val itemEpochTime = it.epochTime ?: throw NoItemEpochTimeException
            val main = it.mainInformation ?: throw NoItemEpochTimeException
            val temperature = main.temperature?.toInt() ?: throw NoMainTemperatureException
            val feelsLikeTemperature = main.feelsLikeTemperature?.toInt() ?: throw NoMainFeelsLikeTemperatureException
            ForecastItem(
                convertTimestampToDateTime(itemEpochTime),
                MainForecast(
                    temperature.toString(),
                    feelsLikeTemperature.toString()
                )
            )
        }

        return CityForecast(
            CityInformation(
                cityId,
                cityName,
                convertTimestampToDate(sunrise),
                convertTimestampToTime(sunrise),
                convertTimestampToTime(sunset)
            ),
            forecastItems
        )
    }

    private fun convertTimestampToDate(timestamp: Long): String {
        val simpleDateFormat = SimpleDateFormat(UI_DATE_FORMAT, Locale.FRANCE)
        val date = Date(timestamp * 1000L)
        return simpleDateFormat.format(date)
    }

    private fun convertTimestampToTime(timestamp: Long): String {
        val simpleDateFormat = SimpleDateFormat(UI_TIME_FORMAT, Locale.FRANCE)
        val date = Date(timestamp * 1000L)
        return simpleDateFormat.format(date)
    }

    private fun convertTimestampToDateTime(timestamp: Long): String {
        val simpleDateFormat = SimpleDateFormat(UI_DATE_TIME_FORMAT, Locale.FRANCE)
        val date = Date(timestamp * 1000L)
        return simpleDateFormat.format(date)
    }

    private companion object {
        const val UI_DATE_FORMAT = "dd/MM/yyyy"
        const val UI_TIME_FORMAT = "HH'h'mm"
        const val UI_DATE_TIME_FORMAT = "dd/MM/yyyy HH'h'mm"
    }

    // Dedicated exceptions are easier to observe in firebase crash stats
    sealed class ProtocolException(message: String = "") : Exception(message) {
        object NoCityException : ProtocolException()
        object NoCityIdException : ProtocolException()
        object NoCityNameException : ProtocolException()
        object NoSunriseEpochTimeException : ProtocolException()
        object NoSunsetEpochTimeException : ProtocolException()
        object NoItemsException : ProtocolException()
        object NoItemEpochTimeException : ProtocolException()
        object NoMainTemperatureException : ProtocolException()
        object NoMainFeelsLikeTemperatureException : ProtocolException()
    }
}