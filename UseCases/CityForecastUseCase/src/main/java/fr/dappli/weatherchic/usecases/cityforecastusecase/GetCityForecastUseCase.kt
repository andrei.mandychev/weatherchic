package fr.dappli.weatherchic.usecases.cityforecastusecase

import fr.dappli.weatherchic.repositories.openweatherrepository.OpenWeatherRepository
import fr.dappli.weatherchic.repositories.openweatherrepository.vo.Definitions
import fr.dappli.weatherchic.usecases.cityforecastusecase.vo.CityForecast
import javax.inject.Inject

class GetCityForecastUseCase @Inject constructor(
    private val repository: OpenWeatherRepository
) : ForecastUseCase() {

    suspend operator fun invoke(cityName: String): CityForecast =
        mapToCityForecast(repository.getWeatherForecast(cityName, Definitions.METRIC, Definitions.DEFAULT_COUNT))
}