package fr.dappli.weatherchic.usecases.cityforecastusecase

import fr.dappli.weatherchic.repositories.openweatherrepository.OpenWeatherRepository
import fr.dappli.weatherchic.repositories.openweatherrepository.vo.Definitions
import fr.dappli.weatherchic.usecases.cityforecastusecase.vo.CityForecast

import javax.inject.Inject

class GetMoreForecastUseCase @Inject constructor(
    private val repository: OpenWeatherRepository
) : ForecastUseCase() {

    suspend operator fun invoke(cityId: String): CityForecast =
        mapToCityForecast(repository.getMoreWeatherForecast(cityId, Definitions.METRIC))

}