package fr.dappli.weatherchic.usecases.cityforecastusecase.vo

data class CityForecast(
    val cityInformation: CityInformation,
    val items: List<ForecastItem>
)