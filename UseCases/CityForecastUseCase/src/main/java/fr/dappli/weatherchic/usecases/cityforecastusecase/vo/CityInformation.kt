package fr.dappli.weatherchic.usecases.cityforecastusecase.vo

data class CityInformation(
    val id: String,
    val name: String,
    val date: String,
    val sunriseTime: String,
    val sunsetTime: String
)