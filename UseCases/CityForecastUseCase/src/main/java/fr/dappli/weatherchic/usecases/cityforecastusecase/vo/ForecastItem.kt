package fr.dappli.weatherchic.usecases.cityforecastusecase.vo

data class ForecastItem(
    val time: String,
    val mainForecast: MainForecast
)