package fr.dappli.weatherchic.usecases.cityforecastusecase.vo

data class MainForecast(
    val temperature: String,
    val feelsLikeTemperature: String
)