package fr.dappli.weatherchic.usecases.cityforecastusecase

import fr.dappli.weatherchic.repositories.openweatherrepository.OpenWeatherRepository
import fr.dappli.weatherchic.repositories.openweatherrepository.vo.Definitions
import fr.dappli.weatherchic.repositories.openweatherrepository.vo.WeatherForecastResponse
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test

import org.junit.Assert.*
import kotlin.random.Random

class GetCityForecastUseCaseTest {

    private val repository = mockk<OpenWeatherRepository>()
    private val getCityForecastUseCase = GetCityForecastUseCase(repository)

    @Test
    fun `test NoCityException`() {
        // Given
        val city = Random.nextInt().toString() // don't care
        coEvery { repository.getWeatherForecast(any(), Definitions.METRIC, Definitions.DEFAULT_COUNT) } returns WeatherForecastResponse(
            Definitions.DEFAULT_COUNT,
            city = null, // NO CITY !!!
            items = emptyList()
        )

        runBlockingTest {
            try {
                // When
                val response = getCityForecastUseCase(city)
                assertTrue(false) // must not happened

            } catch (ex: Exception) {

                // Then
                assertTrue(ex is ForecastUseCase.ProtocolException.NoCityException)
            }
        }

    }

    // TODO add other exception tests
}