package fr.dappli.weatherchic

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import fr.dappli.weatherchic.libraries.navigation.CitySearchNavigator

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val intent = CitySearchNavigator(this).newIntent()
        startActivity(intent)
        finish()
    }

}