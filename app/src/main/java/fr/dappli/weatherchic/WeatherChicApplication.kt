package fr.dappli.weatherchic

import android.app.Application
import fr.dappli.weatherchic.di.inject
import fr.dappli.weatherchic.libraries.core.CoreComponentProvider
import fr.dappli.weatherchic.libraries.core.di.CoreComponent
import fr.dappli.weatherchic.libraries.core.di.DaggerCoreComponent
import fr.dappli.weatherchic.libraries.network.NetworkComponentProvider
import fr.dappli.weatherchic.libraries.network.di.DaggerNetworkComponent
import fr.dappli.weatherchic.libraries.network.di.NetworkComponent

class WeatherChicApplication :
    Application(),
    CoreComponentProvider,
    NetworkComponentProvider
{

    override fun onCreate() {
        super.onCreate()

        inject(this)
    }

    override val coreComponent: CoreComponent by lazy {
        DaggerCoreComponent.factory().create(this)
    }

    override val networkComponent: NetworkComponent by lazy {
        DaggerNetworkComponent.factory().create(coreComponent)
    }

}