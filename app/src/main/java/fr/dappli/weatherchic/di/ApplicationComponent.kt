package fr.dappli.weatherchic.di

import dagger.BindsInstance
import dagger.Component
import fr.dappli.weatherchic.WeatherChicApplication
import fr.dappli.weatherchic.libraries.core.di.CoreComponent
import fr.dappli.weatherchic.libraries.network.di.NetworkComponent

@Component(
    dependencies = [CoreComponent::class, NetworkComponent::class]
)
interface ApplicationComponent {

    fun inject(application: WeatherChicApplication)

    @Component.Factory
    interface Factory {

        fun create(
            coreComponent: CoreComponent,
            networkComponent: NetworkComponent,
            @BindsInstance application: WeatherChicApplication
        ): ApplicationComponent
    }
}