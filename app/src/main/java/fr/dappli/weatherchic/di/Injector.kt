package fr.dappli.weatherchic.di

import fr.dappli.weatherchic.WeatherChicApplication

fun inject(application: WeatherChicApplication) {
    DaggerApplicationComponent
        .factory()
        .create(application.coreComponent, application.networkComponent, application)
        .inject(application)
}